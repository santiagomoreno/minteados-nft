const { ethers }= require("hardhat");

async function main() {
  const Minteados = await ethers.getContractFactory("Minteados");
  const minteados = await Minteados.deploy("Minteados", "MNTDOS");

  await minteados.deployed();

  console.log("minteados deployed to:", minteados.address);

  await minteados.mint("https://ipfs.io/ipfs/QmXXE2ejjWJf3222BKbB7Y1AkNQrf8JKPno8e6aUNGUkzA");
  await minteados.mint("https://ipfs.io/ipfs/QmRuRu29uxfeWMnkKfD6mAutnze9uR4g4tywsqs4WPF5jx");
  await minteados.mint("https://ipfs.io/ipfs/Qmd8kfNog4PLfDmc1LCzXFWnjioddYuxXxPz1oiv7WgSvn");

  console.log("NFTs successfully minted");

}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
